<?php
class ContactPage extends Page {
}
class ContactPage_Controller extends Page_Controller {
    private static $allowed_actions = array('Form');
	
    public function Form() {
    $datefield = new DateField('From', 'Anreise'); 
	$datefield->setConfig('showcalendar', true );
	
	$datefield2 = new DateField('Until', 'Abreise'); 
	$datefield2->setConfig('showcalendar', true );
		 
        $fields = new FieldList( 
            new TextField('Name', 'Name*'), 
            new PhoneNumberField('Phone', 'Telefon'), 
            new EmailField('Email', 'E-Mail*'),
            new DropdownField('Rooms', 'Anzahl Zimmer (Bitte Auswählen)', array('0' => 'Keins', '1' => '1 Zimmer', '2' => '2 Zimmer', '3'=> '3 Zimmer')),
            new DropdownField('Parents', 'Anzahl Erwachsene (Bitte Auswählen)', array('0' => 'Keine', '1' => '1', '2' => '2', '3'=> '3', '4'=> '4', '5'=> '5')),
            new DropdownField('Children-mini', 'Anzahl Kleinkinder (3-9 Jahre) (Bitte Auswählen)', array('0' => 'Keine', '1' => '1', '2' => '2', '3'=> '3', '4'=> '4', '5'=> '5')),
            new DropdownField('Children', 'Anzahl Kinder (10-15 Jahre) (Bitte Auswählen)', array('0' => 'Keine', '1' => '1', '2' => '2', '3'=> '3', '4'=> '4', '5'=> '5')),
            $datefield,
            $datefield2,
            new TextareaField('Message', 'Nachricht*')
        ); 
        $actions = new FieldList( 
            new FormAction('submit', 'Abschicken') 
        ); 
		
		$validator = new RequiredFields('Name', 'Email', 'Message');
        return new Form($this, 'Form', $fields, $actions, $validator);
       
    }
	public function submit($data, $form) { 
        $email = new Email(); 
          
        $email->setTo('berggasthofkranzegg@t-online.de'); 
        $email->setFrom($data['Email']); 
        $email->setSubject("Nachricht von Kontaktformular {$data["Name"]}"); 
          
        $messageBody = " 
            <p><strong>Name:</strong> {$data['Name']}</p>
            <p><strong>E-Mail:</strong> {$data['Email']}</p>
            <p><strong>Telefon:</strong> {$data['Phone']}</p>
            <p><strong>Anzahl Zimmer:</strong> {$data['Rooms']}</p> 
            <p><strong>Erwachsene:</strong> {$data['Parents']}</p>
            <p><strong>Kinder:</strong> {$data['Children']}</p>
            <p><strong>Kleinkinder:</strong> {$data['Children-mini']}</p>
            <p><strong>Anreise:</strong> {$data['From']}</p>
            <p><strong>Abreise:</strong> {$data['Until']}</p> 
            <p><strong>Nachricht:</strong> {$data['Message']}</p> 
        "; 
        $email->setBody($messageBody); 
        $email->send(); 
        return array(
            'Content' => '<p>Vielen Dank für Ihre Nachricht.</p>',
            'Form' => ''
        );
    }
	
	
}