<?php
class HomePage extends Page {
private static $db = array(
		'HomepageTop' => 'Text',
        'HomepageSub' => 'Text',
        'AktuellTitel' => 'Text',
    );
    private static $has_one = array(
        'Photo' => 'Image',

    );
     
    public function getCMSFields() {
        $fields = parent::getCMSFields();
         
        $fields->addFieldToTab("Root.Images", new UploadField('Photo'));
		$fields->addFieldToTab('Root.Main', new TextField('HomepageTop'), 'Content');
		$fields->addFieldToTab('Root.Main', new TextField('HomepageSub'), 'Content');
		$fields->addFieldToTab('Root.Main', new TextField('AktuellTitel'), 'Content');
		         
        return $fields;
    }

}
class HomePage_Controller extends Page_Controller {
}